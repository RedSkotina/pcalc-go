package main

import (
	"bufio"
	"flag"
	"fmt"
	"io"
	"log"
	"math/big"
	"os"
	"runtime"
	"runtime/pprof"
	"sync"
	"time"
)

var (
	Info     *log.Logger
	Error    *log.Logger
	NumCores int
)

func Init(
	infoHandle io.Writer,
	errorHandle io.Writer) {

	Info = log.New(infoHandle,
		"INFO: ", 0)

	Error = log.New(errorHandle,
		"ERROR: ",
		log.Ldate|log.Ltime|log.Lshortfile)
}

type Result struct {
	p    *big.Int
	x    *big.Int
	d    *big.Int
	time time.Duration
}

func (r Result) toString() string {
	var s string
	s = fmt.Sprintf("p=%d x=%d d=%d time=%s\n", r.p, r.x, r.d, r.time)
	return s
}
func (r Result) toStringEx() string {
	var s string
	s = fmt.Sprintf("p=%d x=%d d=%d time=%s\n", r.p, r.x, r.d, r.time)
	return s
}

type IterParam struct {
	p *big.Int
	x *big.Int
}
type IterBatchParam struct {
	arr []*IterParam
}

type IterResult struct {
	cond bool
	p    *big.Int
	x    *big.Int
}

// WARNING: check outbounds
func log2(r *big.Int, x *big.Int) *big.Int {
	return r.SetInt64(int64(x.BitLen() - 1))
}

// warning: only for positive integer powers of 2
func pow2b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), uint(b))
	return
}
func pow4b(r *big.Int, b int64) (ret *big.Int) {
	ret = r.Lsh(r.SetInt64(1), 2*uint(b))
	return
}

func ipxCalc(pch <-chan IterBatchParam, iresults chan<- IterResult, iwg *sync.WaitGroup) {
	defer iwg.Done()

	var p = big.NewInt(0)
	var x = big.NewInt(0)
	var r = big.NewInt(0)

	for ibp := range pch {
		res := new(IterResult)
		for _, ip := range ibp.arr {
			res.x = ip.x
			res.p = ip.p
			p = ip.p
			x = ip.x

			// CALCULATIONS
			r = r.Mod(p, x)
			//
			if r.BitLen() == 0 {
				res.cond = true
				break
			}

		}

		iresults <- *res
	}
}

func pxCalc(jobs <-chan *big.Int, results chan<- Result, wg *sync.WaitGroup) {
	defer wg.Done()
	for p := range jobs {
		start := time.Now()
		Info.Println("start calculation with p =", p.String())
		res := new(Result)
		res.p = p

		pch := make(chan IterBatchParam)
		iresults := make(chan IterResult)
		done := make(chan bool)
		iwg := new(sync.WaitGroup)

		// start  workers
		for w := 1; w <= NumCores; w++ {
			iwg.Add(1)
			go ipxCalc(pch, iresults, iwg)
		}
		// send input values
		go func() {
			b1 := big.NewInt(1)
			x := big.NewInt(1)
			for {
				select {
				case <-done:
					close(pch)
					return
				default:
					ibp := new(IterBatchParam)
					ibp.arr = make([]*IterParam, *batch)
					var i int64
					for i = 0; i < *batch; i++ {
						ip := &IterParam{x: big.NewInt(0).Set(x), p: p}
						ibp.arr[i] = ip
						x.Add(x, b1)
					}
					pch <- *ibp

				}
			}
		}()
		// Collect all results
		go func() {
			iwg.Wait()
			close(iresults)
		}()

		tgp_timer := time.Now()
		btrace := big.NewInt(*trace)
		xb := big.NewInt(0)
		t := big.NewInt(0)
		b1 := big.NewInt(1)
		b2 := big.NewInt(2)
		d := big.NewInt(0)

		done_closed := false
		for r := range iresults {
			xb.Add(xb, b1)
			if *trace != 0 && (t.Mod(xb, btrace).BitLen() == 0) {
				Info.Println("x", r.x, "p", r.p)
				tgp_time := time.Since(tgp_timer)
				// windows timer have 15 ms precision only
				// force 0ms to 15ms
				if tgp_time.Nanoseconds() == 0 {
					tgp_time = time.Duration(15) * time.Millisecond
				}
				Info.Println("throughput x/sec = ", (((*batch)*(*trace))*1e9)/(tgp_time.Nanoseconds()))
				tgp_timer = time.Now()
			}
			if r.cond {
				// sometime we take many true results before close channel
				if !done_closed {
					close(done)
					done_closed = true
				}
				res.p = r.p
				res.x = r.x
				res.d = d.Mul(r.x, b2)
				res.time = time.Since(start)
				results <- *res
			}

		}
	}
}

func processFile(infile string, outfile string) {
	file, err := os.Open(infile)
	if err != nil {
		Error.Println("Could not open file ", infile)
		return
	}
	defer file.Close()

	// unbuffered channels
	jobs := make(chan *big.Int)
	results := make(chan Result)
	// wait group
	wg := new(sync.WaitGroup)
	// start workers
	for w := 1; w <= NumCores; w++ {
		wg.Add(1)
		go pxCalc(jobs, results, wg)
	}

	// scan each line of file and queue up p-number
	go func() {
		defer close(jobs)
		var psl []*big.Int
		scanner := bufio.NewScanner(file)
		for scanner.Scan() {
			p := big.NewInt(0)
			p, suc := p.SetString(scanner.Text(), 10)
			if !suc {
				Error.Println("Cant convert '", scanner.Text(), "' to big.Int with base 10")
				continue
			}
			psl = append(psl, p)
		}
		if len(psl) == 0 {
			return
		}
		for _, p := range psl {
			jobs <- p
		}
	}()

	// Collect all results
	go func() {
		wg.Wait()
		close(results)
	}()

	wch := make(chan Result)
	// output results to file
	go func(wch <-chan Result) {
		wpath := outfile
		wfile, err := os.Create(wpath)
		if err != nil {
			Error.Println("Could not open file ", wpath)
			return
		}
		defer wfile.Close()
		w := bufio.NewWriter(wfile)
		for v := range wch {
			_, err := w.WriteString(v.toStringEx())
			if err != nil {
				Error.Println("Could not write to file ", wpath)
				return
			}
			w.Flush()
		}
	}(wch)

	// send all results from the results channel to the output channel
	for v := range results {
		fmt.Print(v.toString())
		wch <- v
	}
	close(wch)
}

var batch = flag.Int64("batch", 100, "set number of x per series(goroutine)")
var trace = flag.Int64("trace", 100000, "trace results to stdout every N series")
var cpuprofile = flag.String("cpuprofile", "", "write cpu profile to file")
var version = "0.1a"

func main() {
	flag.Parse()
	if *cpuprofile != "" {
		f, err := os.Create(*cpuprofile)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Start cpu profile")
		pprof.StartCPUProfile(f)
		defer pprof.StopCPUProfile()
	}

	Init(os.Stdout, os.Stderr)

	if len(flag.Args()) < 1 {
		fmt.Println("Usage: pcalc [flags] inputfile")
		fmt.Println("flags:")
		flag.PrintDefaults()
		os.Exit(1)
	}
	var infile string = flag.Args()[0]
	Info.Println("Version:", version)

	NumCores = runtime.GOMAXPROCS(-1)
	Info.Println("The number of logical CPUs usable by the current process =", runtime.NumCPU())
	Info.Println("Maximum number of CPUs that can be executing simultaneously =", runtime.GOMAXPROCS(-1))
	Info.Println("Iterations per goroutine(series) = ", *batch)

	start := time.Now()
	processFile(infile, "./out.txt")
	elapsed := time.Since(start)
	Info.Println("Processing took ", elapsed)
}
